'use strict'

const User = use('App/Models/User');

class AuthController {

  async store({ request, response }) {
    const { username, email, password, phone } = request.all();
    const user = await User.create({
      username, email, password, phone
    });
    return response.created({ message: 'Register Successfully', user })
  }

  async login({ request, auth, response }) {
    const { email, password } = request.all();
    const token = await auth.attempt(email, password);
    return response.status(200).json({ message: 'login success', token })
  }

}

module.exports = AuthController
